#ifndef _CKLISTS_H
#define _CKLISTS_H

//#include <converse.h> // for size_t
#include <stdlib.h> // for size_t
#include <string.h> // for memcpy
#include <vector>

//"Documentation" class: prevents people from using copy constructors 
struct CkNoncopyable {
	CkNoncopyable(void) {}
	CkNoncopyable(const CkNoncopyable &c) = delete;
	CkNoncopyable& operator=(const CkNoncopyable &c) = delete;
	CkNoncopyable(CkNoncopyable&&) = default;
	CkNoncopyable& operator=(CkNoncopyable &&) = default;
	~CkNoncopyable() = default;
};

//Implementation class 
template <class T>
class CkSTLHelper {
protected:
    //Copy nEl elements from src into dest
    void elementCopy(T *dest,const T *src,int nEl) {
      for (int i=0;i<nEl;i++) dest[i]=src[i];
    }
};

// Forward declarations:
template <class T> class CkQ;

/// A single-ended FIFO queue.
///  See CkMsgQ if T is a Charm++ message type.
template <class T>
class CkQ {
    std::vector<T> block;
    int first;
    int len;
    int mask;
    void _expand(void) {
      // block.size() is always kept as a power of 2
      int blklen = block.size();
      int newlen = blklen<<1;
      mask |= blklen;
      if (blklen==0) {
	newlen = 16;
	mask = 0x0f;
      }
      block.resize(newlen);
      if (first != 0) {
        // Rearrange elements of block so that first = 0
        std::move(block.begin(), block.begin()+first, block.begin()+blklen);
        std::move(block.begin()+first, block.begin()+blklen, block.begin());
        std::move(block.begin()+blklen, block.begin()+(blklen+first), block.begin()+(blklen-first));
        first = 0;
      }
    }
  public:
    CkQ() : first(0),len(0),mask(0) {}
    CkQ(CkQ &&rhs)
      : block(rhs.block)
      , first(rhs.first)
      , len(rhs.len)
      , mask(rhs.mask)
    {
      rhs.block.clear();
      rhs.len = 0;
    }
    CkQ(int sz) :first(0),len(0) {
      int size = 2;
      mask = 0x03;
      while (1<<size < sz) { mask |= 1<<size; size++; }
      int blklen = 1<<size;
      block.resize(blklen);
    }
    CkQ(const CkQ&) = delete;
    void operator=(const CkQ&) = delete;
    void operator=(CkQ&&) = delete; // Assignment of one queue to another makes no sense
    ~CkQ() {}
    inline int length() const { return len; }
    inline int isEmpty() const { return (len==0); }
    T deq() {
      if(len>0) {
        T &ret = block[first];
	first = (first+1)&mask;
        len--;
      	return ret;
      } else return T(); //For builtin types like int, void*, this is equivalent to T(0)
    }
    void enq(const T &elt) {
      if(len==block.size()) _expand();
      block[(first+len)&mask] = elt;
      len++;
    }
    // stack semantics, needed to replace FIFO_QUEUE of converse
    void push(const T &elt) {
      if(len==block.size()) _expand();
      first = (first-1+block.size())&mask;
      block[first] = elt;
      len++;
    }
    // stack semantics: look at the element on top of the stack
    inline T& peek() {
      return block[first];
    }
    // insert an element at pos.
    void insert(int pos, const T &elt) {
      while(len==block.size() || pos>=block.size()) _expand();
      for (int i=len; i>pos; i--)
        block[(i+first)&mask] = std::move(block[(i-1+first)&mask]);
      block[(pos+first)&mask] = elt;
      if (pos > len) len = pos+1;
      else len++;
    }
    // delete an element at pos
    T remove(int pos) {
      if (pos >= len) return T(0);
      T ret = block[(pos+first)&mask];
      for (int i=pos; i<len-1; i++)
        block[(i+first)&mask] = std::move(block[(i+1+first)&mask]);
      len--;
      return ret;
    }
    // delete all elements from pos
    inline void removeFrom(int pos) {
      len = pos;
    }
    //Peek at the n'th item from the queue
    inline T& operator[](size_t n)
    {
    	n=(n+first)&mask;
    	return block[n];
    }
    // needed to replace FIFO_Enumerate
    T* getArray(void) {
      T *newblk = new T[len];
      int i,j;
      for(i=0,j=first;i<len;i++){
        newblk[i] = block[j];
        j = (j+1)&mask;
      }
      return newblk;
    }
};


#endif
