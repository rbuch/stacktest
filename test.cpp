#include "cklists.h"
#include <stack>
#include <iostream>
#include <chrono>

typedef std::chrono::high_resolution_clock Clock;

#define TRIALS 100000000

class Test {
  int value;

public:
  Test() {}
  Test(int input) {
    value = input;
  }
};

int main() {
  CkQ<Test> ckqstack;

  auto start = Clock::now();
  for (int i = 0; i < TRIALS; ++i)
  {
    ckqstack.push(Test(i));
    ckqstack.deq();
  }
  auto end = Clock::now();
  std::cout << "CkQStack: " << std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count() << std::endl;

  std::stack<Test, std::vector<Test>> regstacknoemplace;
  start = Clock::now();
  for (int i = 0; i < TRIALS; ++i)
  {
    regstacknoemplace.push(i);
    regstacknoemplace.pop();
  }
  end = Clock::now();
  std::cout << "regstacknoemplace: " << std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count() << std::endl;
  
  std::stack<Test, std::vector<Test>> regstack;
  start = Clock::now();
  for (int i = 0; i < TRIALS; ++i)
  {
    regstack.emplace(i);
    regstack.pop();
  }
  end = Clock::now();
  std::cout << "regstack: " << std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count() << std::endl;
  
}
